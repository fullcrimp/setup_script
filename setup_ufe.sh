#!/usr/bin/env bash
FOLDER="$HOME/sourcecode/Sephora/ufe/"

print() {
    echo -e "\n*********************************************************************"
    echo "$1"
    echo -e "*********************************************************************\n"
}

set -e
function cleanup {
    print "STOPPING NODE PROCESSES"
    npm run stop
}
trap cleanup EXIT


cd $FOLDER

# check if branch name is passed as a param
if [ -z "$1" ]
then 
    print "STAYING ON THE SAME BRANCH"
elif [ -n "$1" ]
then  
    print "CHECKING OUT BRANCH $1"
    git checkout $1
fi

# check for uncommited changes
if [[ -z $(git status -s) ]]
then
    echo -e "$FOLDER \nHAS NO UNCOMMITED CHANGES"
else
    echo -e "$FOLDER \nHAS UNCOMMITED CHANGES"
    exit
fi

print "UPDATE NPM DEPENDENCIES"
npm update

cleanup

print "STARTING SERVER IN FRONT-END RENDER MODE"
npm run start-develop

print "SETTING UP WEBPACK WATCH"
npm run webpack-frontend



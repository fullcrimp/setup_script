#!/usr/bin/env bash

cd $HOME
osascript -e 'tell application "Terminal" to do script ". setup_ufe.sh '"$1"'"'
osascript -e 'tell application "Terminal" to do script ". setup_mobile-web.sh '"$1 $2"'"'

# open vscode
open /Applications/Visual\ Studio\ Code.app


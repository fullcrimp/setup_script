## Everyday local environment setup automation for mac users

- Copy the files to $HOME folder

- Run as `. setup.sh branch_name env_name` where `branch_name` is the same branch name for UFE and Mobile-web, and e.g. `env_name` is an environment name 

        . setup.sh 2018.7 qa2
    
    If branch names for UFE and Mobile-web are not the same scripts should be started separately:
    

        . setup_ufe.sh ILLUPH-115009-113836-google-recaptcha
        . setup_mobile-web.sh 2018.7 qa2

- Processes will be opened in separate terminals
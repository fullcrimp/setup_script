#!/usr/bin/env bash
  
FOLDER="$HOME/sourcecode/Sephora/mobile-web/"

print() {
    echo -e "\n*********************************************************************"
    echo "$1"
    echo -e "*********************************************************************\n"
}

cd $FOLDER

# check if branch name is passed as a param
if [ -z "$1" ]
then
    print "STAYING ON THE SAME BRANCH"
elif [ -n "$1" ]
then
    print "CHECKING OUT BRANCH $1"
    git checkout $1
fi

# check for uncommited changes
if [ -z $(git status -s) ]
then
    echo -e "$FOLDER \nHAS NO UNCOMMITED CHANGES"
else
    echo -e "$FOLDER \nHAS UNCOMMITED CHANGES"
    exit
fi

print "PULLING LAST CHANGES"
git pull

print "REINSTALL DEPENDENCIES"
mvn clean package

print "STARTING SERVER IN FRONT-END RENDER MODE"
./run.sh $2
